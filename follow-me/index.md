---
title: Compose Graphics 2
---

We continue our Graph editor application, adding support for

* Blinking like-typed shapes when clicked
* Dragging shapes
* Drawing lines between objects (that stay connected when the shapes are moved)

Note that I walk through several intentional errors during these videos and explain why they occur/how to fix them. The user tap/drag interaction can be trickier to get right than it seems, so I wanted to highlight common problems and show that sometimes an "obvious" design may not be correct...

!!! note

    I really like the videos for this module, so I'm not converting them to text. I have added some
    notes to some of the videos for some minor changes, but the vast majority of the content works
    as-is.

## Videos

Total video time for this module: 1:57:28

!!! note

    You can change the quality and playback speed by clicking the :material-cog: icon when the video is playing. 
    If you would like to search the captions for this video, click _Watch on YouTube_, press "..." and choose _Show Transcript_.

### Jetpack Compose Graphics: Blinking and Internal Compiler Errors (Fall 2021) (52:55)

!!! note

    A few notes on the content of this video:

    7:25 - The compiler error has been fixed, but this section might
           still be interesting to demonstrate some debug/narrowing techniques
           when faced with a compiler error

    15:40 - Note that nested functions can be tricky inside composables,
            and it's better to avoid them. You really can't test them either.

    24:30 - I completely reimplemented the tapping/dragging.
            
    Originally I was trying to
    do multiple gesture handlers in a `pointerInput`, which doesn't work.
    I can't recall if I tried multiple `pointerInputs` in the original
    example or not. The docs at the time didn't make this clear
    (and/or it didn't work),
    so I ended up tweaking the gesture detectors themselves.
    The new version adds multiple `pointerInputs` where needed.
    This part of the video is still interesting, though, due to some of
    the explanations on how taps, drags and such work, especially
    the "touch slop" discussion.
    Note that you probably will never need to modify the gesture detectors
    as shown in the video; you just need multiple `pointerInput` modifiers.

    47:45 - should be using collectAsStateWithLifecycle
    
    48:20 - I changed the example to use viewModelScope in the view model to launch
            the coroutine


<iframe width="800" height="450" src="https://www.youtube.com/embed/4UZt1KOd8ow" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
             
### Jetpack Compose Graphics: Dragging and Connecting (Fall 2021) (1:04:33)

!!! note

    A few notes on the content of this video:

    00:00 - I completely reimplemented the tapping/dragging. See the notes on the first video for details

    Still worth watching this part so you'll understand how user input maps to teh function calls.

    17:38 - the Handlers instance will actually get recreated whenever the collected flows emit new values as well

    23:40 - another way to fix this problem is to use rememberUpdatedState. This creates indirect access to the
    value to be used inside lambdas that shouldn't be restarted/re-evaluated.


<iframe width="800" height="450" src="https://www.youtube.com/embed/SqscYM8G8KY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            
## Example Source

Source code for examples is at
[https://gitlab.com/605-686/fall-2023-refresh/modules/compose-graphics-2](https://gitlab.com/605-686/fall-2023-refresh/modules/compose-graphics-2)