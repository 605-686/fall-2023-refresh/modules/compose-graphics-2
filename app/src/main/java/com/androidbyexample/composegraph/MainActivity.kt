package com.androidbyexample.composegraph

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.PathEffect
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.Fill
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.translate
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.androidbyexample.composegraph.ui.theme.ComposeGraphTheme

class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<GraphViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeGraphTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colorScheme.background) {
                    Ui(viewModel)
                }
            }
        }
    }
}

data class Handlers(
    val onAddShape: (Shape) -> Unit,
    val onToolChange: (ToolType) -> Unit,
    val onHighlightShape: (finger: Offset, size: Float) -> Unit,
    val onDragStart: (finger: Offset, size: Float) -> Unit,
    val onDrag: (Offset) -> Unit,
    val onDragEnd: () -> Unit,
    val onLineStart: (finger: Offset, size: Float) -> Unit,
    val onLineEnd: (finger: Offset, size: Float) -> Unit,
    val onPress: (finger: Offset) -> Unit,
)

@Composable
fun Ui(
    viewModel: GraphViewModel
) {
    val selectedTool by viewModel.selectedTool.collectAsStateWithLifecycle(initialValue = Square)
    val highlightShapeType by viewModel.highlightShapeType.collectAsStateWithLifecycle(initialValue = null)
    val shapes by viewModel.shapes.collectAsStateWithLifecycle(initialValue = emptyList())
    val lines by viewModel.lines.collectAsStateWithLifecycle(initialValue = emptyList())

    val handlers = remember(viewModel) {
        Handlers(
            onAddShape = viewModel::add,
            onToolChange = viewModel::select,
            onDragStart = viewModel::startDrag,
            onDrag = viewModel::drag,
            onDragEnd = viewModel::endDrag,
            onHighlightShape = viewModel::highlightShape,
            onLineStart = viewModel::startLine,
            onLineEnd = viewModel::endLine,
            onPress = viewModel::onPress,
        )
    }

    Graph(
        shapeSizeDp = 36.dp,
        shapeOutlineWidthDp = 3.dp,
        shapeBoxSizeDp = 48.dp,
        shapes = shapes,
        lines = lines,
        highlightShapeType = highlightShapeType,
        selectedTool = selectedTool,
        handlers = handlers,
        modifier = Modifier.fillMaxSize()
    )
}

fun List<Shape>.findWithId(id: String) =
    firstOrNull { it.id == id }
        ?: throw IllegalStateException("Internal Error: Id $id not found for any shapes")

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Graph(
    shapeSizeDp: Dp,
    shapeOutlineWidthDp: Dp,
    shapeBoxSizeDp: Dp,
    shapes: List<Shape>,
    lines: List<Line>,
    highlightShapeType: ShapeType?,
    selectedTool: ToolType,
    handlers: Handlers,
    modifier: Modifier
) {
    with(LocalDensity.current) {
        val dashLengthPx = 6.dp.toPx()
        val dashGapPx = 3.dp.toPx()
        val shapeSizePx = shapeSizeDp.toPx()
        val shapeOutlineWidthPx = shapeOutlineWidthDp.toPx()
        val shapeBoxSizePx = shapeBoxSizeDp.toPx()
        val shapeOffsetPx = (shapeBoxSizePx - shapeSizePx)/2
        val radius = shapeSizePx/2
        val highlightedBorderColor = MaterialTheme.colorScheme.primary
        val normalBorderColor = Color.Black

        val shapeSize = remember(shapeSizePx) {
            Size(shapeSizePx, shapeSizePx)
        }
        val shapeBoxSize = remember(shapeBoxSizePx) {
            Size(shapeBoxSizePx, shapeBoxSizePx)
        }
        val halfShapeBoxOffset = remember(shapeBoxSizePx) {
            Offset(shapeBoxSizePx/2, shapeBoxSizePx/2)
        }
        val shapeCenter = remember(shapeSizePx) {
            Offset(shapeSizePx/2, shapeSizePx/2)
        }
        val outline = remember(shapeOutlineWidthDp) {
            Stroke(shapeOutlineWidthPx)
        }
        val dashedOutline = remember(shapeOutlineWidthDp) {
            Stroke(
                shapeOutlineWidthPx,
                pathEffect = PathEffect.dashPathEffect(floatArrayOf(dashLengthPx, dashGapPx))
            )
        }
        var finger by remember { mutableStateOf(Offset.Zero) }

        fun Shape.getCenter() = offset + halfShapeBoxOffset

        val trianglePath = remember(shapeSizePx) {
            Path().apply {
                moveTo(shapeSizePx/2, 0f)
                lineTo(shapeSizePx, shapeSizePx)
                lineTo(0f, shapeSizePx)
                close()
            }
        }
        fun DrawScope.drawTriangle(x: Float, y: Float, outlineColor: Color) {
            translate(x + shapeOffsetPx, y + shapeOffsetPx) {
                drawPath(path = trianglePath, color = Color.Red)
                drawPath(path = trianglePath, color = outlineColor, style = outline)
            }
        }
        fun DrawScope.drawSquare(x: Float, y: Float, outlineColor: Color, fill: Boolean = true, stroke: Stroke = outline) {
            translate(x + shapeOffsetPx, y + shapeOffsetPx) {
                if (fill) {
                    drawRect(color = Color.Blue, topLeft = Offset.Zero, size = shapeSize, style = Fill)
                }
                drawRect(color = outlineColor, topLeft = Offset.Zero, size = shapeSize, style = stroke)
            }
        }
        fun DrawScope.drawCircle(x: Float, y: Float, outlineColor: Color) {
            translate(x + shapeOffsetPx, y + shapeOffsetPx) {
                drawCircle(color = Color.Green, center = shapeCenter, radius = radius, style = Fill)
                drawCircle(color = outlineColor, center = shapeCenter, radius = radius, style = outline)
            }
        }
        fun DrawScope.drawLine(x: Float, y: Float, outlineColor: Color) {
            translate(x, y) {
                val lineY = halfShapeBoxOffset.y
                drawLine(
                    color = outlineColor,
                    strokeWidth = shapeOutlineWidthPx,
                    start = Offset(shapeOffsetPx, lineY),
                    end = Offset(shapeBoxSizePx - shapeOffsetPx, lineY)
                )
            }
        }

        Scaffold(
            topBar = {
                TopAppBar(
                    colors = TopAppBarDefaults.mediumTopAppBarColors(
                        containerColor = MaterialTheme.colorScheme.primary,
                    ),
                    title = { Text(stringResource(R.string.graph)) },
                    actions = {
                        ToolbarButton(
                            toolType = Square,
                            shapeBoxSize = shapeBoxSize,
                            selectedTool = selectedTool,
                            onToolChange = handlers.onToolChange,
                            draw = { x, y -> drawSquare(x, y, normalBorderColor) }
                        )
                        ToolbarButton(
                            toolType = Circle,
                            shapeBoxSize = shapeBoxSize,
                            selectedTool = selectedTool,
                            onToolChange = handlers.onToolChange,
                            draw = { x, y -> drawCircle(x, y, normalBorderColor) }
                        )
                        ToolbarButton(
                            toolType = Triangle,
                            shapeBoxSize = shapeBoxSize,
                            selectedTool = selectedTool,
                            onToolChange = handlers.onToolChange,
                            draw = { x, y -> drawTriangle(x, y, normalBorderColor) }
                        )
                        ToolbarButton(
                            toolType = DrawLine,
                            shapeBoxSize = shapeBoxSize,
                            selectedTool = selectedTool,
                            onToolChange = handlers.onToolChange,
                            draw = { x, y -> drawLine(x, y, normalBorderColor) }
                        )
                        ToolbarButton(
                            toolType = Select,
                            shapeBoxSize = shapeBoxSize,
                            selectedTool = selectedTool,
                            onToolChange = handlers.onToolChange,
                            draw = { x, y -> drawSquare(x, y, outlineColor = normalBorderColor, fill = false, stroke = dashedOutline) }
                        )
                    }
                )
            },
            content = { paddingValues ->

                // pulled modifier out of Canvas to remember it, recomputing when the
                //   selected tool or padding values change
                val canvasModifier = remember(selectedTool, paddingValues) {
                    val paddedModifier = modifier.padding(paddingValues)
                    when (selectedTool) {
                        DrawLine ->
                            paddedModifier
                                .pointerInput(shapeBoxSizePx) {
                                    detectDragGestures(
                                        onDragStart = {
                                            handlers.onLineStart(it, shapeBoxSizePx)
                                        },
                                        onDrag = { change, _ ->
                                            finger = change.position
                                        },
                                        onDragEnd = {
                                            handlers.onLineEnd(finger, shapeBoxSizePx)
                                        },
                                        onDragCancel = {
                                            handlers.onLineEnd(finger, shapeBoxSizePx)
                                        },
                                    )
                                }

                        Select ->
                            paddedModifier
                                .pointerInput(selectedTool, halfShapeBoxOffset) {
                                    detectTapGestures(
                                        onTap = {
                                            handlers.onHighlightShape(it, shapeBoxSizePx)
                                        },
                                        onPress = {
                                            // capture the finger offset
                                            handlers.onPress(it)
                                        },
                                    )
                                }
                                // register drag handler
                                .pointerInput(selectedTool, halfShapeBoxOffset) {
                                    detectDragGestures(
                                        onDragStart = { offset ->
                                            handlers.onDragStart(offset, shapeBoxSizePx)
                                        },
                                        onDrag = { change, _ ->
                                            handlers.onDrag(change.position)
                                        },
                                        onDragEnd = {
                                            handlers.onDragEnd()
                                        },
                                        onDragCancel = {
                                            handlers.onDragEnd()
                                        },
                                    )
                                }

                        is ShapeType ->
                            paddedModifier
                                .pointerInput(selectedTool, halfShapeBoxOffset) {
                                    detectTapGestures(
                                        onTap = {
                                            handlers.onAddShape(
                                                Shape(
                                                    shapeType = selectedTool,
                                                    offset = it - halfShapeBoxOffset
                                                )
                                            )
                                        }
                                    )
                                }
                    }
                }

                Canvas(
                    modifier = canvasModifier
                ) {
                    lines.forEach { line ->
                        drawLine(
                            color = Color.DarkGray,
                            strokeWidth = shapeOutlineWidthPx,
                            start = shapes.findWithId(line.shape1Id).getCenter(),
                            end = line.shape2Id?.let { shapes.findWithId(it).getCenter() } ?: finger,
                        )
                    }
                    shapes.forEach {
                        val outlineColor = if (highlightShapeType == it.shapeType)
                            highlightedBorderColor
                        else
                            normalBorderColor

                        when(it.shapeType) {
                            Circle -> drawCircle(it.offset.x, it.offset.y, outlineColor)
                            Square -> drawSquare(it.offset.x, it.offset.y, outlineColor)
                            Triangle -> drawTriangle(it.offset.x, it.offset.y, outlineColor)
                        }
                    }
                }
            }
        )
    }
}

@Composable
fun ToolbarButton(
    toolType: ToolType,
    shapeBoxSize: Size,
    selectedTool: ToolType,
    onToolChange: (ToolType) -> Unit,
    draw: DrawScope.(Float, Float) -> Unit
) {
    val backgroundColor = MaterialTheme.colorScheme.onPrimary
    Canvas(
        modifier = Modifier.size(48.dp)
            .clickable { onToolChange(toolType) }
    ) {
        if (selectedTool == toolType) {
            drawRect(color = backgroundColor, topLeft = Offset.Zero, size = shapeBoxSize)
        }
        draw(0f,0f)
    }
}

